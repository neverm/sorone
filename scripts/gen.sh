#!/bin/bash

# This is a "generator script" that is only run to generate the actual test cases.
# Use it to guide AI so that it knows the structure and can generate more cases

# Create directories for test cases
mkdir -p $DATA_DIR/arithmetic
mkdir -p $DATA_DIR/lists
mkdir -p $DATA_DIR/functions

# Create arithmetic test files
echo "(+ 1 2)" > $DATA_DIR/arithmetic/addition.scm
echo "(- 5 3)" > $DATA_DIR/arithmetic/subtraction.scm
echo "(* 4 2)" > $DATA_DIR/arithmetic/multiplication.scm
echo "(/ 10 2)" > $DATA_DIR/arithmetic/division.scm
echo "(+ 1.5 2.5)" > $DATA_DIR/arithmetic/float_addition.scm

# Create list operation test files
echo "(cons 1 (quote (2 3)))" > $DATA_DIR/lists/cons.scm
echo "(car (quote (1 2 3)))" > $DATA_DIR/lists/car.scm
echo "(cdr (quote (1 2 3)))" > $DATA_DIR/lists/cdr.scm
echo "(list 1 2 3)" > $DATA_DIR/lists/creation.scm
echo "(length (quote (1 2 3 4)))" > $DATA_DIR/lists/length.scm

# Create function test files
echo "((lambda (x) (+ x 2)) 3)" > $DATA_DIR/functions/lambda.scm
echo "(begin (define (square x) (* x x)) (square 4))" > $DATA_DIR/functions/square.scm
echo "(begin (define (double x) (* x 2)) (double 3))" > $DATA_DIR/functions/double.scm
echo "(begin (define (f x) (if (< x 0) -x x)) (f -5))" > $DATA_DIR/functions/absolute.scm
echo "(begin (define (sum a b) (+ a b)) (sum 2 3))" > $DATA_DIR/functions/sum.scm

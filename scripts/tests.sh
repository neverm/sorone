#!/bin/bash
# for now, some basic tests that check if I didn't break anything

check() {
    expected="$1"
    actual="$2"

    if [ "$actual" = "$expected" ]; then
        echo "Test passed!"
    else
        echo "Test failed: expected '$expected', actual '$actual'"
    fi
}

check_piped() {
    input="$1"
    expected="$2"
    echo "Checking on input $1"
    result=$(echo "$1" | ./sor)
    check "$expected" "$result"
}

check_piped "1" "1"
check_piped "(+ 1 2)" "3"
check_piped "(car (quote (1 2 3)))" "1"
check_piped "(cdr (quote (1 2 3)))" "(2 3)"

check_file() {
    echo "Running file $1"
    filename="$1"
    expected="$2"
    result=$(./sor "$filename")
    check "$expected" "$result"
}

DATA_DIR="$(pwd)/scripts/test_data"

check_file "$DATA_DIR/math/add.scm" "10"
check_file "$DATA_DIR/math/sub.scm" "-4"
check_file "$DATA_DIR/math/neg.scm" "-5"

check_file "$DATA_DIR/define/names.scm" "15"

# AI generated, regenerate as needed

# Check arithmetic test cases
check_file "$DATA_DIR/arithmetic/addition.scm" "3"
check_file "$DATA_DIR/arithmetic/subtraction.scm" "2"
check_file "$DATA_DIR/arithmetic/multiplication.scm" "8"
check_file "$DATA_DIR/arithmetic/division.scm" "5"
# check_file "$DATA_DIR/arithmetic/float_addition.scm" "4.0"

# Check list operation test cases
# check_file "$DATA_DIR/lists/cons.scm" "(1 2 3)"
check_file "$DATA_DIR/lists/car.scm" "1"
check_file "$DATA_DIR/lists/cdr.scm" "(2 3)"
# check_file "$DATA_DIR/lists/creation.scm" "(1 2 3)"
# check_file "$DATA_DIR/lists/length.scm" "4"

# Check function test cases
check_file "$DATA_DIR/functions/lambda.scm" "5"
# check_file "$DATA_DIR/functions/square.scm" "16"
# check_file "$DATA_DIR/functions/double.scm" "6"
# check_file "$DATA_DIR/functions/absolute.scm" "5"
# check_file "$DATA_DIR/functions/sum.scm" "5"
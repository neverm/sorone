# Dependencies


###  Libedit

`sudo apt install libedit-dev`



# Todo things

- trace all allocations, understand why do we copy things so much
- try unit tests
- rewrite list (sexp) implementation from array to linked list
- rewrite value to be a tagged union
- code organization
    1. Move list (sexp) out and make it abstract so that performance can be measured for array and linked list approach
- implement parser
- implement gc
- ctrl+d results in segfault, investigate
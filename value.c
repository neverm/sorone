#include "value.h"
#include "error.h"
#include "eval.h"
#include "utils.h"
#include "value_type.h"

value nothing = { .type = VAL_NOTHING };

char *type_name(value_type type) {
    switch (type) {
    case VAL_FLOAT:
        return "float";
    case VAL_INT:
        return "int";
    case VAL_ERR:
        return "errpr";
    case VAL_SEXP:
        return "list";
    case VAL_BUILTIN_FUN:
        return "function";
    case VAL_NOTHING:
        return "##nothing";
    case VAL_LAMBDA:
        return "lambda";
    default:
        return "unknown";
    }
}

// --------------------
// Creating values
// --------------------

// todo: rewrite all value_* functions so that allocation happens outside and we return success status

// todo: trace all allocations starting from reading, to evaluation and to printing result
// make sure all paths have proper deallocation, including error cases
value *value_int(long x) {
    value* v = malloc(sizeof(value));
    if (v == NULL) {
        FAIL("error allocating integer");
    }
    v->type = VAL_INT;
    v->ival = x;
    return v;
}

value *value_float(double x) {
    value* v = malloc(sizeof(value));
    if (v == NULL) {
        FAIL("error allocating float");
    }
    v->type = VAL_FLOAT;
    v->fval = x;
    return v;
}

value *value_err(Error *err) {
    value* v = malloc(sizeof(value));
    if (v == NULL) {
        FAIL("error allocating error");
    }
    v->type = VAL_ERR;
    v->err = err;
    return v;
}

value *value_sym(const char *sym) {
    value* v = malloc(sizeof(value));
    if (v == NULL) {
        FAIL("error allocating symbol");
    }
    v->type = VAL_SYM;
    v->sym = malloc(strlen(sym)+1);
    strcpy(v->sym, sym);
    return v;
}

value *value_sexp(void) {
    value* v = malloc(sizeof(value));
    if (v == NULL) {
        FAIL("error allocating list");
    }
    v->type = VAL_SEXP;
    v->count = 0;
    v->cells = NULL;
    return v;
}

value *value_builtin_func(env *e, builtin_fn f) {
    value* v = malloc(sizeof(value));
    if (v == NULL) {
        FAIL("error allocating builtin func");
    }
    v->type = VAL_BUILTIN_FUN;
    v->fn = f;
    v->env = e;
    return v;
}

value *value_lambda(env *e, int argc, char **params, value *body) {
    value *v = malloc(sizeof(value));
    if (v == NULL) {
        FAIL("error allocating lambda");
    }
    v->env = e;
    v->argnum = argc;
    v->params = params;
    v->body = body;
    return v;
}

value *deepcopy(value *v) {
    value *result;
    switch (v->type) {
    case VAL_NOTHING:
        // statically allocated single global instance, no need to copy
        return v;
    case VAL_INT:
        return value_int(v->ival);
    case VAL_FLOAT:
        return value_float(v->fval);
    case VAL_ERR:
        // fixme: is it ok to shallow-copy the actual error?
       return value_err(v->err);
    case VAL_SYM:
        return value_sym(v->sym);
    case VAL_BUILTIN_FUN:
        return value_builtin_func(v->env, v->fn);
    case VAL_LAMBDA:
        // fixme: for now, do we want to copy lambdas? 
        return v;
    case VAL_SEXP:
        result = value_sexp();
        result->type = v->type;
        for (int i = 0; i < v->count; i++) {
            sexp_add(result, deepcopy(v->cells[i]));
        }
        return result;
    default:
        return RUNTIME_ERROR("unrecognized type: %s", type_name(v->type));
    }
}

void delete_value(value *v) {
    switch (v->type) {
    case VAL_NOTHING:
        return;
    case VAL_INT:
    case VAL_FLOAT:
    case VAL_BUILTIN_FUN:
        break;
    case VAL_SYM:
        free(v->sym);
        break;
    case VAL_ERR:
        delete_error(v->err);
        break;
    case VAL_LAMBDA:
        // fixme: memory leak for now, need to think how to share lambda values. Or switch to copy?
        break;
    case VAL_SEXP:
        for (int i = 0; i < v->count; i++) {
            delete_value(v->cells[i]);
        }
        free(v->cells);
    }
    free(v);
}

// --------------------
// Printing values
// --------------------

void print_sexp(value *v) {
    putchar('(');
    for (int i = 0; i < v->count; i++) {
        print_value(v->cells[i]);
        if (i != v->count-1) {
            putchar(' ');
        }
    }
    putchar(')');
}

void print_value(value *v) {
    if (v == NULL) {
        printf("Value is null");
        return;
    }
    switch (v->type) {
    case VAL_NOTHING:
        return;
    case VAL_INT:
        printf("%li", v->ival);
        break;
    case VAL_FLOAT:
        printf("%f", v->fval);
        break;
    case VAL_SYM:
        printf("%s", v->sym);
        break;
    case VAL_SEXP:
        print_sexp(v);
        break;
    case VAL_LAMBDA:
        printf("<function>");
        break;
    case VAL_BUILTIN_FUN:
        printf("<builtin function>");
        break;
    case VAL_ERR:
        print_err(v->err);
        break;
    }
}

void println_value(value *v) {
    print_value(v);
    if (v->type != VAL_NOTHING) {
        putchar('\n');
    }
}

// --------------------
// Reading
// Basically, construct a *value out of ast
// In case of lists, recursively read children
// --------------------

value *read_value(mpc_ast_t *t) {
    if (strstr(t->tag, "integer")) { return read_int(t); }
    if (strstr(t->tag, "float")) { return read_float(t); }
    if (strstr(t->tag, "symbol")) { return value_sym(t->contents); }
    
    /* If root (>) or sexp then create nothing list */
    value *v = NULL;
    // t will have 3 children, 0 and 2 are regex and 1 is the actual child
    // when the parser is changed to something sane, it will get rewritten
    if (strcmp(t->tag, ">") == 0) { return read_value(t->children[1]); }
    if (strstr(t->tag, "sexp"))  { v = value_sexp(); }

    if (v == NULL) {
        puts(t->tag);
        return RUNTIME_ERROR("parse error");
    }

    for (int i = 0; i < t->children_num; i++) {
        if (strcmp(t->children[i]->contents, "(") == 0) { continue; }
        if (strcmp(t->children[i]->contents, ")") == 0) { continue; }
        if (strcmp(t->children[i]->tag,  "regex") == 0) { continue; }
        v = sexp_add(v, read_value(t->children[i]));
    }
    return v;
}

value *read_int(mpc_ast_t *t) {
    errno = 0;
    long x = atoi(t->contents);
    if (errno != ERANGE) {
        return value_int(x);
    }
    return RUNTIME_ERROR("bad number format");
}

value *read_float(mpc_ast_t *t) {
    errno = 0;
    double x = atof(t->contents);
        if (errno != ERANGE) {
        return value_float(x);
    }
    return RUNTIME_ERROR("bad number format");
}

// sexp operations

// todo: rewrite using linked list?
value *sexp_add(value *sexp, value *v) {
    sexp->count++;
    // reallocate array of cells to a size one more than it was before
    // this func was called
    sexp->cells = realloc(sexp->cells, sizeof(value*) * sexp->count);
    // put new value to the end of cells array
    sexp->cells[sexp->count-1] = v;
    return sexp;
}

value *sexp_pop(value *sexp, int pos) {
    value *x = sexp->cells[pos];

    sexp->count--;
    // move (sexp->count-pos) elements, starting from the element pos+1
    // one element to the left
    memmove(&sexp->cells[pos], &sexp->cells[pos+1], sizeof(value *) * (sexp->count-pos));
    // todo: terribly ineffcient to reallocate every time
    sexp->cells = realloc(sexp->cells, sizeof(value *) * sexp->count);
    return x;
}

value *validate_list_type(value *lst, value_type expected) {
    if (lst->type != VAL_SEXP) {
        // todo: change to type error
        return RUNTIME_ERROR("expected list got something else");
    }
    for (int i = 0; i < lst->count; i++) {
        // todo: change to type error
        if (lst->cells[i]->type != expected) {
            return RUNTIME_ERROR("unexpected type");
        }
    }
    return NULL;
}
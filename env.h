#ifndef ENV_H
#define ENV_H

#include "value.h"

typedef struct env env;

env *new_env(void);
void del_env(env *e);
void env_set(env *e, const char *name, value *val);
value *env_get(env *e, char *name);

#endif

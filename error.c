#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"
#include "utils.h"

Error *runtime_error(char *fmt, ...) {
    Error* err = malloc(sizeof(Error));
    err->type = RUNTIME_ERROR;
    va_list va;
    va_start(va, fmt);

    // allocate 512 bytes first
    char *msg = (char *) malloc(512);

    // format the string and write up to 511 bytes of text into err
    // fixme: there could be a bug here I think, that when there is exactly 511 bytes of text, the
    // \0 terminator won't be written and has to be manually set
    vsnprintf(msg, 511, fmt, va);
    // the actual string size could be less, so reallocate using strlen
    msg = realloc(msg, strlen(msg)+1);
    // fixme: of course, malloc and realloc could fail
    va_end(va);

    RuntimeError re = { .message = msg };
    err->data.runtime = re;
    return err;
}

void delete_error(Error *err) {
    switch (err->type) {
    case ARGUMENT_ERROR:
        FAIL("argument error deallocation not implemented")
        break;
    case TYPE_ERROR:
        FAIL("type error deallocation not implemented")
        break;
    case RUNTIME_ERROR:
        free(err->data.runtime.message);
        free(err);
        break;
    }
}

void print_err(Error *err) {
    switch (err->type) {
    case ARGUMENT_ERROR:
        FAIL("argument error printing not implemented")
        break;
    case TYPE_ERROR:
        FAIL("type error printing not implemented")
        break;
    case RUNTIME_ERROR:
        printf("runtime error: %s", err->data.runtime.message);
    }
}

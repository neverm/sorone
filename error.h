#ifndef ERROR_H
#define ERROR_H

#include "value_type.h"

typedef enum ErrorType {
    ARGUMENT_ERROR,
    TYPE_ERROR,
    RUNTIME_ERROR,
} ErrorType;

typedef struct ArgumentError {
    int expected;
    int actual;
} ArgumentError;

typedef struct TypeError {
    value_type expected;
    value_type actual;
} TypeError;

typedef struct RuntimeError {
    char *message;
} RuntimeError;

typedef struct Error {
    ErrorType type;
    union data {
        ArgumentError arg;
        TypeError type;
        RuntimeError runtime;
    } data;
} Error;

Error *runtime_error(char *fmt, ...);
Error *type_error(value_type expected, value_type actual);
Error *argument_error(int expected, int actual);

void print_err(Error *err);

void delete_error(Error *err);

#endif

#include "builtins.h"
#include "eval.h"

// binary_op is a binary operation: a function th
typedef value*(*binary_op)(value *, value *);

value *fold_right(int count, value **vals, binary_op op);

value *add(value *x, value *y);
value *sub(value *x, value *y);
value *divide(value *x, value *y);
value *mul(value *x, value *y);
value *min(value *x, value *y);
value *max(value *x, value *y);
value *rem(value *x, value *y);
value *power(value *x, value *y);

value *head(value *qexp);
value *tail(value *qexp);
value *join(int count, value **items);

value *builtin_minus(int count, value **vals);
value *builtin_add(int count, value **vals);
value *builtin_div(int count, value **vals);
value *builtin_mul(int count, value **vals);
value *builtin_min(int count, value **vals);
value *builtin_max(int count, value **vals);
value *builtin_rem(int count, value **vals);
value *builtin_power(int count, value **vals);
value *builtin_car(int count, value **vals);
value *builtin_cdr(int count, value **vals);

void add_builtin(env *e, const char *name, builtin_fn f) {
    value *fn_val = value_builtin_func(e, f);
    env_set(e, name, fn_val);
}

void init_builtins(env *e) {
    add_builtin(e, "-", &builtin_minus);
    add_builtin(e, "+", &builtin_add);
    add_builtin(e, "*", &builtin_mul);
    add_builtin(e, "/", &builtin_div);
    add_builtin(e, "min", &builtin_min);
    add_builtin(e, "max", &builtin_max);
    add_builtin(e, "rem", &builtin_rem);
    add_builtin(e, "pow", &builtin_power);
    add_builtin(e, "car", &builtin_car);
    add_builtin(e, "cdr", &builtin_cdr);
}

// todo: fix RUNTIME_ERROR calls where possible, replace with more
// appropriate error types

value *fold_right(int count, value **vals, binary_op op) {
    if (count < 2) {
        return RUNTIME_ERROR("expected more than one argument");
    }
    value *result = vals[0];
    for (int i = 1; i < count; i++) {
        result = op(result, vals[i]);
    }
    return result;
}

value *builtin_minus(int count, value **vals) {
    if (count == 0) {
        return RUNTIME_ERROR("expected one or more arguments");
    }
    if (count == 1) {
        value *x = vals[0];
        if (x->type == VAL_FLOAT) {
            return value_float(-1 * x->fval);
        }
        if (x->type == VAL_INT) {
            return value_int(-1 * x->ival);
        }
    }
    
    return fold_right(count, vals, &sub);
}

value *builtin_add(int count, value **vals) {
    return fold_right(count, vals, &add);
}

value *builtin_div(int count, value **vals) {
    return fold_right(count, vals, &divide);
}

value *builtin_mul(int count, value **vals) {
    return fold_right(count, vals, &mul);
}

value *builtin_min(int count, value **vals) {
    return fold_right(count, vals, &min);
}

value *builtin_max(int count, value **vals) {
    return fold_right(count, vals, &max);
}



value *builtin_rem(int count, value **vals) {
    if (count == 2) {
        return RUNTIME_ERROR("expected two arguments");
    }
    value *x = vals[0];
    value *y = vals[1];
    if (x->type != VAL_INT || y->type != VAL_INT) {
        return RUNTIME_ERROR("incorrect argument types");
    }
    return value_int(x->ival % y->ival);
}

value *builtin_power(int count, value **vals) {
    if (count == 2) {
        return RUNTIME_ERROR("expected two arguments");
    }
    value *x = vals[0];
    value *y = vals[1];
    if (x->type == VAL_FLOAT && y->type == VAL_FLOAT) {
        return value_float(pow(x->fval, y->fval));
    }
    if (x->type == VAL_INT && y->type == VAL_INT) {
        return value_int(pow(x->ival, y->ival));
    }
    return RUNTIME_ERROR("incorrect argument types");
}

value *add(value *x, value *y) {
    if (x->type == VAL_FLOAT && y->type == VAL_FLOAT) {
        return value_float(x->fval + y->fval);
    }
    if (x->type == VAL_INT && y->type == VAL_INT) {
        return value_int(x->ival + y->ival);
    }
    return RUNTIME_ERROR("incorrect argument types");
}

value *sub(value *x, value *y) {
    if (x->type == VAL_FLOAT && y->type == VAL_FLOAT) {
        return value_float(x->fval - y->fval);
    }
    if (x->type == VAL_INT && y->type == VAL_INT) {
        return value_int(x->ival - y->ival);
    }
    return RUNTIME_ERROR("incorrect argument types");
}

value *mul(value *x, value *y) {
    if (x->type == VAL_FLOAT && y->type == VAL_FLOAT) {
        return value_float(x->fval * y->fval);
    }
    if (x->type == VAL_INT && y->type == VAL_INT) {
        return value_int(x->ival * y->ival);
    }
    return RUNTIME_ERROR("incorrect argument types");
}

value *divide(value *x, value *y) {
    if (x->type == VAL_FLOAT && y->type == VAL_FLOAT) {
        if (y->fval == 0.0) {
            return RUNTIME_ERROR("division by zero");
        }
        return value_float(x->fval / y->fval);
    }
    if (x->type == VAL_INT && y->type == VAL_INT) {
        if (y->ival == 0) {
            return RUNTIME_ERROR("division by zero");
        }
        return value_int(x->ival / y->ival);
    }
    return RUNTIME_ERROR("incorrect argument types");
}

value *min(value *x, value *y) {
    if (x->type == VAL_FLOAT && y->type == VAL_FLOAT) {
        return (x->fval < y->fval)
            ? value_float(x->fval)
            : value_float(y->fval);
    }
    if (x->type == VAL_INT && y->type == VAL_INT) {
    return (x->ival < y->ival)
            ? value_int(x->ival)
            : value_int(y->ival);
    }
    return RUNTIME_ERROR("incorrect argument types");
}

value *max(value *x, value *y) {
    if (x->type == VAL_FLOAT && y->type == VAL_FLOAT) {
        return (x->fval > y->fval)
            ? value_float(x->fval)
            : value_float(y->fval);
    }
    if (x->type == VAL_INT && y->type == VAL_INT) {
        return (x->ival > y->ival)
            ? value_int(x->ival)
            : value_int(y->ival);
    }
    return RUNTIME_ERROR("incorrect argument types");
}

// --------------------
// List operations
// --------------------

value *builtin_car(int count, value **vals) {
    if (count != 1) {
        return RUNTIME_ERROR("car: expected one argument, got %d", count);
    }
    value *lst = vals[0];
    if (lst->type != VAL_SEXP) {
		return RUNTIME_ERROR("incorrect argument type: expected list, got ...");
	}
	if (lst->count == 0) {
		return RUNTIME_ERROR("empty list");
	}
	return deepcopy(lst->cells[0]);
}

value *builtin_cdr(int count, value **vals) {
	if (count != 1) {
        return RUNTIME_ERROR("car: expected one argument, got %d", count);
    }
    value *lst = vals[0];
    if (lst->type != VAL_SEXP) {
		return RUNTIME_ERROR("incorrect argument type: expected list, got ...");
	}
	if (lst->count == 0) {
		return RUNTIME_ERROR("empty list");
	}
    sexp_pop(lst, 0);
	return deepcopy(lst);
}

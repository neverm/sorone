CC = gcc
LIBS = -ledit -lm
CFLAGS = $(LIBS) -Wall -std=c99 -g
SOR_OBJ = sor.o mpc.o eval.o builtins.o value.o env.o error.o

# ifeq ($(DEBUG), 1)
# 	CFLAGS += -DDEBUG=1
# endif

# make .o files from the .c files that have the same basename
%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

sor: $(SOR_OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

debug: CFLAGS += -DDEBUG=1 -g
debug: sor

test: sor
	./scripts/tests.sh

.PHONY: clean test

clean:
	rm -rf *.o


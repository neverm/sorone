#include "value.h"
#include "env.h"

struct env {
    int count;
    char **syms;
    value **vals;
    env *parent;
};

// --------------------
// Allocation
// As of now, everything is allocated forever, until environment is destroyed
// Everything is copied:
//  - getting a value from env returns a copy of the value in env
//  - setting a value to env actually sets a copy
// This is wasteful, but no need to implement garbage collection for now, so can concentrate on other
// parts like evaluation
// --------------------

env *new_env(void) {
    env *e = malloc(sizeof(env));
    e->count = 0;
    e->syms = NULL;
    e->vals = NULL;
    e->parent = NULL;
    return e;
}

void del_env(env *e) {
    for (int i = 0; i < e->count; i++) {
        free(e->syms[i]);
        delete_value(e->vals[i]);
    }
    free(e->syms);
    free(e->vals);
    free(e);
}

value *env_get(env *e, char *name) {
    for (int i = 0; i < e->count; i++) {
        if (strcmp(name, e->syms[i]) == 0) {
            // todo: find out if it's an issue that we return a copy, and possibly
            // a better way for this, since we only need one copy of immutable data
            return deepcopy(e->vals[i]);
        }
    }
    if (e->parent != NULL) {
        return env_get(e->parent, name);
    }
    return RUNTIME_ERROR("%s: undefined", name);
}

void env_set(env *e, const char *name, value *val) {
    // if the name is already bound to some value, delete that value first
    for (int i = 0; i < e->count; i++) {
        if (strcmp(name, e->syms[i]) == 0) {
            delete_value(e->vals[i]);
            e->vals[i] = deepcopy(val);
            return;
        }
    }
    int idx = e->count;
    e->count++;
    e->vals = realloc(e->vals, sizeof(value *) * e->count);
    e->syms = realloc(e->syms, sizeof(char *) * e->count);
    e->vals[idx] = deepcopy(val);
    e->syms[idx] = malloc(strlen(name) + 1);
    strcpy(e->syms[idx], name);
}

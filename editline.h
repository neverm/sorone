#ifndef EDIT_H
#define EDIT_H
#ifdef _WIN32
#include <string.h>

static char *buffer[2048];

char *readline(char *prompt) {
  fputs(prompt, stdout);
  fgets(buffer, 2048, stdin);
  char *cpy = malloc(strlen(buffer)+1);
  strcpy(cpy, buffer);
  // delete trailing newline
  cpy[strlen(cpy)-1] = '\0';
}

// fake function
void add_history(char *unused) {}

#else
#include <editline/readline.h>
#endif

#endif

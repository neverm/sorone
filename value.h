#ifndef VALUE_H
#define VALUE_H

#include "mpc.h"
#include "value_type.h"
#include "error.h"

// forward declaration to break dependency cycle between env and value
typedef struct env env;

// forward declaration for builtin_fn
typedef struct value value;

typedef value*(*builtin_fn)(int count, value **vals);

// todo: rewrite using unions
// todo: rewrite sexp using linked list?
typedef struct value {
    value_type type;

    // integer value
    long ival;

    // double value
    double fval;

    // error value
    Error *err;

    // symbol value
    char *sym;

    // builtin function
    builtin_fn fn;

    // lambda
    env *env;
    int argnum;
    char **params;
    // unevaluated body
    value *body;

    // list value: array of *value
    int count;
    struct value **cells;
} value;

extern value nothing;

// reading
value *read_value(mpc_ast_t *t);
value *read_int(mpc_ast_t *t);
value *read_float(mpc_ast_t *t); 

// constructors
value *value_int(long x);
value *value_float(double x);
value *value_sym(const char *sym);
value *value_sexp(void);
value *value_builtin_func(env *e, builtin_fn f);
value *value_lambda(env *e, int argc, char **params, value *body);

// error values
value *value_err(Error *err);
#define RUNTIME_ERROR(...) value_err(runtime_error(__VA_ARGS__));

// memory
value *deepcopy(value *v);
void delete_value(value *v);

// list operations
value *sexp_pop(value *sexp, int pos);
value *sexp_add(value *sexp, value *v);

// printing
void print_value(value *v);
void println_value(value *v);

// validate that:
// lst is a list
// each element of lst has the expected type
// return NULL if no error, or value of type Error
value *validate_list_type(value *lst, value_type expected);

#endif

#ifndef EVAL_H
#define EVAL_H
#include <stdbool.h>
#include "mpc.h"
#include "value.h"
#include "env.h"

#define ASSERT(args, cond, fmt...) \
    if( !(cond) ) { \
        lval* err = value_err(fmt, ##__VA_ARGS__); \
        delete_value(args); \
        return err; \
    }

value *eval(env *e, value *v);

// builtin forms
value *define(env *e, value *sexp);
value *quote(env *e, value *sexp);
value *begin(env *e, value *sexp);
value *lambda(env *e, value *sexp);

#endif

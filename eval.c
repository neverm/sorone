#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include "mpc.h"
#include "eval.h"
#include "utils.h"
#include "value.h"
#include "value_type.h"

value *eval_application(env *e, value *v);

// --------------------
// Evaluation
// - evaluate symbols by looking them up in the passed env
// - atomic values and empty lists to themselves: 3 -> 3
// - define form is evaluated for its effect, and returned value is `nothing'
// - quote does not evaluate its body, but returns it as it is (as a list)
// - a list is evaluated as a function application
// Note: the value *v given as parameter MIGHT be deleted after evaluation, so make a copy
// if you intend to use *v again
// --------------------

value *eval(env *e, value *v) {
    DEBUGF("eval");
    // if it's a symbol, look it up in the passed env
    if (v->type == VAL_SYM) {
        value *x = env_get(e, v->sym);
        delete_value(v);
        return x;
    }
    if (v->type == VAL_NOTHING) {
    // empty is only returned as a result of define
        return v;
    }
    // atomic values are evaluated to themselves
    if (v->type != VAL_SEXP) {
        return v;
    }
    // everything else is a list
    return eval_application(e, v);
}

#define LIST_HEAD_IS(list, value) \
    (list->cells[0]->type == VAL_SYM && strcmp(list->cells[0]->sym, value) == 0)

// eval_application is the core of the evaluator. It evaluates special forms like define,
// as well as handles generic function application in form (op arg1 arg2 arg3 ...)
// The evaluation proceeds as follows:
// evaluate every subexpression in the list sequentially, i.e. op, then arg1, arg2, etc
// apply the op to the arguments arg1 arg2, ...
value *eval_application(env *e, value *v) {
    DEBUGF("eval_application");
    // empty list '() cannot be evaluated
    if (v->count == 0) {
        delete_value(v);
        return RUNTIME_ERROR("cannot evaluate `()', missing procedure expression");
    }
    // built-in forms
    if (LIST_HEAD_IS(v, "define")) {
        return define(e, v);
    }
    if (LIST_HEAD_IS(v, "quote")) {
        return quote(e, v);
    }
    // evaluate lambda FORM. This is not application of a lambda, but a list in form
    // (lambda (<params) <body>)
    // This will evaluate the FORM. Result of the evaluation will be a lambda/closure object,
    if (LIST_HEAD_IS(v, "lambda")) {
        return lambda(e, v);
    }
    if (LIST_HEAD_IS(v, "begin")) {
        return begin(e, v);
    }
    // additional built-in forms go here, e.g. let, begin
    // ...

    // Apply

    // Evaluate generic sexp (f a b c ...): an application of a function f to arguments a b c ...
    // this is basically (apply f (a b c ...))
    
    // first we evaluate all elements of the list: f, a, b, c, ... and replace each
    // with the result of corresponding evaluation. If any of them result in error, return that error.
    // Evaluation goes left to right
    DEBUGF("apply")
    for (int i = 0; i < v->count; i++) {
        v->cells[i] = eval(e, v->cells[i]);
        if (v->cells[i]->type == VAL_ERR) {
            value *err = sexp_pop(v, i);
            delete_value(v);
            return err;
        }
    }
    
    // Check that the operation f is callable. At this point we reject inputs like
    // (1 2 3), which is application of 1 to arguments 2 and 3
    value *op = sexp_pop(v, 0);
    value *result;
    switch (op-> type) {
        case VAL_BUILTIN_FUN:
            // apply function to remaining items in the list
            result = op->fn(v->count, v->cells);
            delete_value(op);
            delete_value(v);
            return result;
        case VAL_LAMBDA:
            return RUNTIME_ERROR("applying lambda will be here");
        default:
            delete_value(op);
            delete_value(v);
            return RUNTIME_ERROR("expected a procedure or lambda that can be applied to arguments");
    }
}

value *define(env *e, value *form) {
    if (form->count != 3) {
        return RUNTIME_ERROR("unexpected number of arguments to define: %d, expected: 2", form->count);
    }
    value *name = form->cells[1];
    if (name->type != VAL_SYM) {
        delete_value(form);
        return RUNTIME_ERROR("expected symbol, got: %d", name->type);
    }
    value *stored = env_get(e, name->sym);
    // errors are not first-class citizens, we use them inside the interpreter only, even though they are values
    // So, if env_get returned something else than VAL_ERR, we assume that it was a success
    // todo: add env_has, or rethink how errors work.
    // Specifically, in this case is it guaranteed that when env_get returns something of VAL_ERR, it
    // is a lookup error?
    if (stored->type != VAL_ERR) {
        delete_value(form);
        return RUNTIME_ERROR("name %s is already defined", name->sym);
    }

    value *body = sexp_pop(form, 2);
    body = eval(e, body);
    if (body->type == VAL_ERR) {
        delete_value(form);
        return body;
    }
    DEBUGF("storing value into env");
    env_set(e, name->sym, body);
    delete_value(body);
    delete_value(form);
    return &nothing;
}

value *quote(env *e, value *form) {
    if (form->count != 2) {
        return RUNTIME_ERROR("unexpected number of arguments to quote: %d, expected: 1", form->count);
    }
    // todo: can we just do pop here instead of deepcopy?
    value *cp = deepcopy(form->cells[1]);
    delete_value(form);
    return cp;
}

value *begin(env *e, value *form) {
    if (form->count < 2) {
        return RUNTIME_ERROR("malformed begin: expected at least one expression");
    }
    value *result = NULL;
    for (int i = 1; i < form->count; i++) {
        result = eval(e, form->cells[i]);
        if (result->type == VAL_ERR) {
            return result;
        }
        // if it's not the last expression, we are not gonna return it, so delete it.
        // Usually, these are "side effect" expressions like define
        if (i < form->count-1) {
            delete_value(result);
        }
    }
    // todo: investigate
    // currently, the caller of eval frees the allocation of `form`. Not sure if it's correct, or even looks good
    // Maybe it's okay: whatever we are given we don't touch, but we deallocate everything we produced and no
    // intend to return
    // delete_value(form);
    return result;
}

// evaluate lambda expression. The result would be a lambda object (closure) (also value *),
// which holds reference to the env it was created in.
value *lambda(env *e, value *form) {
    DEBUGF("starting lambda");
    // for simplicity, disallow lambdas that have many expressions in their body.
    // They can be replaced with a single expression that wraps everything in a `begin`.
    if (form->count != 3 || form->cells[1]->type != VAL_SEXP) {
        return RUNTIME_ERROR("lambda: invalid syntax");
    }
    DEBUGF("remove lambda symbol");
    // remove the "lambda" symbol
    sexp_pop(form, 0);
    value *params = sexp_pop(form, 0);
    DEBUGF("validating list type");
    value *err = validate_list_type(params, VAL_SYM);
    if (err != NULL) {
        return err;
    }
    DEBUGF("allocating lambda");
    value *lam = malloc(sizeof(value));
    lam->argnum = params->count;
    // copy all symbol pointers from the respective symbolic value *, into just flat
    // array of pointers.
    // todo: will this cause a memory leak? Effectively I just transfer "ownership" of the memory for
    // the symbols from the value *, into the lambda.
    DEBUGF("processing parameters");
    char **param_names = calloc(params->count, sizeof(char *));
    for (int i = 0; i < params->count; i++) {
        param_names[i] = params[i].sym;
    }
    lam->params = param_names;

    DEBUGF("moving body");
    value *body = sexp_pop(form, 0);
    lam->body = body;
    lam->type = VAL_LAMBDA;
    delete_value(form);
    
    return lam;
}

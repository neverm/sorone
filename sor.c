#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <unistd.h>

#include "editline.h"
#include "mpc.h"
#include "eval.h"
#include "builtins.h"
#include "utils.h"

void run_repl(env *env, mpc_parser_t *parser);
void run_piped(env *env, mpc_parser_t *parser);
void run_file(env *env, mpc_parser_t *parser, char *filename);

int main(int argc, char **argv) {
    DEBUGF("starting");
    mpc_parser_t *Integer  = mpc_new("integer");
    mpc_parser_t *Float  = mpc_new("float");
    mpc_parser_t *Number  = mpc_new("number");
    mpc_parser_t *Symbol  = mpc_new("symbol");
    mpc_parser_t *Sexp  = mpc_new("sexp");
    mpc_parser_t *Exp = mpc_new("exp");
    mpc_parser_t *Sor = mpc_new("sor");

    mpca_lang(MPCA_LANG_DEFAULT,
                        "float    : /-?[0-9]+\\.[0-9]+/ ;"
                        "integer  : /-?[0-9]+/ ;"
                        "number   : <float> | <integer> ;"
                        "symbol   : /[a-zA-Z0-9_+\\-*\\/\\\\=<>!&]+/ ; "
                        "sexp    : '(' <exp>* ')' ; "
                        "exp : <number> | <symbol> | <sexp> ;"
                        "sor : /^/ <exp> /$/; ",
                        Integer, Float, Number, Symbol,
                        Sexp, Exp, Sor, NULL
                    );

    env *global = new_env();
    init_builtins(global);

    if (argc > 1) {
        run_file(global, Sor, argv[1]);
    } else if (isatty(fileno(stdin))) {
        run_repl(global, Sor);
    } else {
        run_piped(global, Sor);
    }

    mpc_cleanup(7, Integer, Float, Number, Symbol,
                        Sexp, Exp, Sor);
    del_env(global);
    return EXIT_SUCCESS;
}

void run_repl(env *env, mpc_parser_t *parser) {
    printf("Sor version 0.0.1\n");
    printf("Press Ctrl+C to exit\n");
    mpc_result_t res;
    while (1) {
        char *input = readline("sor> ");
        add_history(input);
        if (mpc_parse("input", input, parser, &res)) {
            value *v = read_value(res.output);
            v = eval(env, v);
            println_value(v);
            delete_value(v);
            // mpc_ast_print(res.output);
            mpc_ast_delete(res.output);
        } else {
            mpc_err_print(res.error);
            mpc_err_delete(res.error);
        }
        free(input);
    }
}

// todo: piped and file mode only support single expression for now because the parser
// generator is retarded.
// When our own parser is rolled, switch to that and allow reading multiple expressions evaluating each
// in turn

// As a workaround, implement and use `begin` form. (begin exp1 exp2 exp3 ...)

void run_piped(env *env, mpc_parser_t *parser) {
    mpc_result_t res;
    char buffer[256];
    if (fgets(buffer, sizeof(buffer), stdin) != NULL) {
        // Remove newline character if present
        buffer[strcspn(buffer, "\n")] = '\0';
        if (mpc_parse("input", buffer, parser, &res)) {
            value *v = read_value(res.output);
            v = eval(env, v);
            println_value(v);
            delete_value(v);
            // mpc_ast_print(res.output);
            mpc_ast_delete(res.output);
        } else {
            mpc_err_print(res.error);
            mpc_err_delete(res.error);
        }
    } else {
        printf("Error reading from stdin.\n");
    }
}

void run_file(env *env, mpc_parser_t *parser, char *filename) {
    char *content = NULL;
    FILE *fp;
    fp = fopen(filename, "r");
    if (fp) {
        fseek(fp, 0, SEEK_END);
        size_t size = ftell(fp);
        content = malloc(size);
        fseek(fp, 0, SEEK_SET);

        fread(content, 1, size, fp);

        fclose(fp);
    }
    if (content != NULL) {
        mpc_result_t res;
        if (mpc_parse("input", content, parser, &res)) {
            value *v = read_value(res.output);
            v = eval(env, v);
            println_value(v);
            delete_value(v);
            // mpc_ast_print(res.output);
            mpc_ast_delete(res.output);
        } else {
            mpc_err_print(res.error);
            mpc_err_delete(res.error);
        }
    } else {
        // todo: examine errno or whatever, complain to stderr
        printf("couldn't read the file");
    }
    if (content != NULL) {
        // don't really need to, but good practice I guess
        free(content);
    }
}
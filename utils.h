#ifndef UTILS_H
#define UTILS_H

#ifndef DEBUG
#define DEBUG 0
#endif

#define FAIL(...) \
    fprintf(stderr, __VA_ARGS__); \
    exit(1);

#if DEBUG
#define DEBUGF(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__);
#else
#define DEBUGF(fmt, ...)
#endif // DEBUG


#endif // UTILS_H

#ifndef VALUE_TYPE_H
#define VALUE_TYPE_H

typedef enum value_type {
    VAL_INT,
    VAL_FLOAT,
    VAL_ERR,
    VAL_SYM,
    VAL_SEXP,
    VAL_BUILTIN_FUN,
    VAL_LAMBDA,
    VAL_NOTHING,
} value_type;

#endif
